package com.test.springboot.springboottest.springboottest.model;

import javax.persistence.*;

@Entity
@Table( name = "vehicles")
public class Vehicles {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private long id;
    @Column(name="name")
    private String name;

    public Vehicles(){}

    public Vehicles(long id, String name) {
        this.id = id;
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }
}
