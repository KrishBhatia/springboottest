package com.test.springboot.springboottest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.message.*;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
public class Application {

    @Bean
    public RestTemplate restTemplate(RestTemplateBuilder builder){
        return builder.build();
    }

    private static final Logger logger = LogManager.getLogger();

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);

        logger.debug("debug--Message");
        logger.info("Info--Message");
//        logger.warn("Warn--Message");
//        logger.error("Error--Message");
//        logger.fatal("Fatal--Message");
    }

}
