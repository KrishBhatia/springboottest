package com.test.springboot.springboottest.springboottest.exception;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.io.Serial;

@ResponseStatus(value = HttpStatus.NO_CONTENT)
public class ResourceNotFound extends Exception{
    @Serial
    private static final long serialVersionUID = 1L;
    private static final Logger logger = LogManager.getLogger();

    public ResourceNotFound(String message){
        super(message);
        logger.info(message);
    }
}
